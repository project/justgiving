# drupal module to wrap the justgiving api
-----------------------------------------


CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


## Introduction.

This module  covers
  * creating a JustGiving account
  * create a JustGiving event event
  * create JustGiving page for the event
  * retrieve details from a JustGiving page
  * retrieve leaderboard for charity

## Requirements.

A JustGiving api account
Libraries

## Installation.

Grab the justgiving api from here:

https://github.com/JustGiving/JustGiving.Api.Sdk

store this in the libraries folder for your site.

you can do this by doing the following
(on linux or mac)

```
cd sites/all/libraries
git clone https://github.com/JustGiving/JustGiving.Api.Sdk
ln -s  JustGiving.Api.Sdk justgiving
```

or by copying the PHP folder to be called justgiving.

It should look like this:

```
libraries/justgiving/JustGivingClient.php
libraries/justgiving/ApiClients
```

## Configuration
You need to configure the module with your credentials here:

```
admin/config/justgiving
```

These you get from the JustGiving api.


## Sub modules

### Leaderboard
Creates a block for the top 10 fundraisers for the charity

## Maintainers

created and maintained by tim_marsh at Positive
                http://www.positivestudio.co.uk
maintained by i-sibbot at Positive
                http://www.positivestudio.co.uk

This project has been sponsored by:

 * Positive
   We’re a digital agency.
   We often work with charity, non-profit, public sector and education clients,
   although not exclusively.
   Visit http://www.positivestudio.co.uk for more information.
